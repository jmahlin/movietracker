class DirectorsController < ApplicationController
  def index
    @directors = current_user.movies.collect(&:directors).flatten
  end

  def show
    @director = Director.find(params[:id])
    @movies = @director.movies
  end

  def search
  end
end
